import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GroupBox {
    width: 200
    height: 200
    font.pointSize: 16
    font.bold: true
    Layout.fillWidth: true
    title: qsTr("C")

    property alias cLayout: cLayout
    property alias speech: button5.text
    property alias weak: button6.text
    
    RowLayout {
        id: rowLayout
        anchors.fill: parent
        
        ColumnLayout {
            id: columnLayout3
            width: 100
            height: 100
            
            Text {
                id: text3
                text: qsTr("Clinical features of the TIA")
                font.pointSize: 16
            }
        }
        
        ColumnLayout {
            id: cLayout
            width: 100
            height: 100
            
            Button {
                id: button4
                text: qsTr("Other symptoms")
                Layout.fillWidth: true
                checked: true
                checkable: true
            }
            
            Button {
                id: button5
                text: "Speech disturbance without weakness +1"
                Layout.fillWidth: true
                checkable: true
            }
            
            Button {
                id: button6
                text: qsTr("Unilateral weakness +2")
                checkable: true
                Layout.fillWidth: true
            }
        }
    }
}
