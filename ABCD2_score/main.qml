import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Window {
    id: window
    visible: true
    //iPhone7
    width: 750
    height: 1334
    
    title: qsTr("ABCD² Score for TIA")

    Logic {
        id: logic
    }
    
    ColumnLayout {
        anchors.fill: parent
        transformOrigin: Item.Center
        
        Score {
            id: score
        }

        ABox {
            id: aBox
        }
        
        BBox {
            id: bBox
        }
        
        CBox {
            id: cBox
        }

        D1Box {
            id: d1Box
        }
        
        D2Box {
            id: d2Box
        }
        
    }
    
    ButtonGroups {
        id: buttonGroups
    }

}
