import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GroupBox {
    width: 200
    height: 200
    font.pointSize: 16
    font.bold: true
    font.family: "Arial"
    Layout.fillWidth: true
    title: qsTr("A")
    
    property alias aLayout: aLayout
    property alias no: button0.text

    RowLayout {
        id: aLayout
        anchors.fill: parent
        
        ColumnLayout {
            id: columnLayout2
            width: 100
            height: 100
            
            Text {
                id: text1
                text: qsTr("Age ≥ 60 years")
                font.pointSize: 16
            }
        }
        
        Button {
            id: button0
            text: qsTr("No (0)")
            checked: true
            highlighted: false
        }
        
        Button {
            id: button1
            text: qsTr("Yes (+1)")
            checkable: true
        }
        
    }
}
