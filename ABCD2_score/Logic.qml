import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    property int a: 0
    property int b: 0
    property int c: 0
    property int d1: 0
    property int d2: 0
    property int s: 0
    
    property variant risk: ["low", "medium", "high"]
    
    property string lowShade: "palegreen"
    property string modShade: "yellow"
    property string hiShade: "red"
    
    property string lowInfo: qsTr(
                                 "Per the validation study, 0-3 points: Low Risk
            2-Day Stroke Risk: 1.0%
            7-Day Stroke Risk: 1.2%
            90-Day Stroke Risk: 3.1%")
    property string modInfo: qsTr(
                                 "Per the validation study, 4-5 points: Moderate Risk
            2-Day Stroke Risk: 4.1%
            7-Day Stroke Risk: 5.9%
            90-Day Stroke Risk: 9.8%")
    property string hiInfo: qsTr(
                                "Per the validation study, 6-7 points: High Risk
             2-Day Stroke Risk: 8.1%
             7-Day Stroke Risk: 11.7%
             90-Day Stroke Risk: 17.8%")
    
    function recalculate() {
        s = a + b + c + d1 + d2
        if (s < 4) {
            state = "low"
        }
        else if (s < 6) {
            state = "medium"
        }
        else {
            state = "high"
        }
    }
    
    states: [
        State {
            name: "low"
            PropertyChanges {target: score; title: "Score " + logic.s + " points: Low Risk"}
            PropertyChanges {target: score; riskShade: logic.lowShade}
            PropertyChanges {target: score; riskInfo: logic.lowInfo}
        },
        State {
            name: "medium"
            PropertyChanges {target: score; title: "Score " + logic.s + " points: Moderat Risk"}
            PropertyChanges {target: score; riskShade: logic.modShade}
            PropertyChanges {target: score; riskInfo: logic.modInfo}
        },
        State {
            name: "high"
            PropertyChanges {target: score; title: "Score " + logic.s + " points: High Risk"}
            PropertyChanges {target: score; riskShade: logic.hiShade}
            PropertyChanges {target: score; riskInfo: logic.hiInfo}
        }
    ]
    
}
