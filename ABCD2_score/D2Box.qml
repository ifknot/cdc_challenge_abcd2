import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GroupBox {
    width: 200
    height: 200
    font.pointSize: 16
    font.bold: true
    Layout.fillHeight: false
    Layout.fillWidth: true
    title: qsTr("D2")

    property alias d2Layout: d2Layout
    property alias no: button10.text
    
    RowLayout {
        id: d2Layout
        anchors.fill: parent
        
        ColumnLayout {
            id: columnLayout2
            width: 100
            height: 100
            
            Text {
                id: text5
                text: qsTr("History of diabetes")
                font.pointSize: 16
            }
        }
        
        Button {
            id: button10
            text: qsTr("No (0)")
            checked: true
            checkable: true
        }
        
        Button {
            id: button11
            text: qsTr("Yes (+1)")
            checkable: true
        }
    }
}
