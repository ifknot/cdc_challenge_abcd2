import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GroupBox {
    width: 200
    height: 200
    font.pointSize: 16
    font.bold: true
    Layout.fillWidth: true
    title: qsTr("Score 0 points")

    property alias riskShade: rect.color
    property alias riskInfo: info.text

    RowLayout {
        id: rowLayout3
        anchors.fill: parent
        
        Rectangle {
            id: rect
            color: "white"
            anchors.fill: parent
        }

        Text {
            id: info
            text: qsTr("")
            font.pointSize: 8
        }
    }
}
