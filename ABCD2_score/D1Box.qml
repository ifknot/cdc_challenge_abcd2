import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GroupBox {
    width: 200
    height: 200
    font.bold: true
    font.pointSize: 16
    Layout.fillWidth: true
    title: qsTr("D1")

    property alias d1Layout: d1Layout
    property alias under60: button8.text
    property alias over60: button9.text
    
    RowLayout {
        id: rowLayout1
        anchors.fill: parent
        
        ColumnLayout {
            id: columnLayout4
            width: 100
            height: 100
            
            Text {
                id: text4
                text: qsTr("Duration of symptoms")
                font.pointSize: 16
            }
        }
        
        ColumnLayout {
            id: d1Layout
            width: 100
            height: 100
            
            Button {
                id: button7
                text: qsTr("<10 minutes ")
                Layout.fillWidth: true
                checked: true
                checkable: true
            }
            
            Button {
                id: button8
                text: qsTr("10-59 minutes +1")
                checkable: true
                Layout.fillWidth: true
            }
            
            Button {
                id: button9
                text: qsTr("≥ 60 minutes +2")
                checkable: true
                Layout.fillWidth: true
            }
        }
    }
    
}
