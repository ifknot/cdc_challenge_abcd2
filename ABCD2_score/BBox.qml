import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

GroupBox {
    width: 200
    height: 200
    font.pointSize: 16
    font.bold: true
    Layout.fillWidth: true
    title: qsTr("B")

    property alias bLayout: bLayout
    property alias no: button2.text
    
    RowLayout {
        id: bLayout
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0
        anchors.fill: parent
        
        ColumnLayout {
            id: columnLayout1
            width: 100
            height: 100
            
            Text {
                id: text2
                text: qsTr("BP ≥ 140/90 mmHg")
                font.pointSize: 16
            }
            
            Text {
                id: text6
                text: qsTr("Initial BP Either SBP ≥ 140 or DBP ≥ 90")
                font.pixelSize: 12
            }
        }
        
        Button {
            id: button2
            text: qsTr("No (0)")
            checked: true
            checkable: true
        }
        
        Button {
            id: button3
            text: qsTr("Yes (+1)")
            checkable: true
        }
    }
}
