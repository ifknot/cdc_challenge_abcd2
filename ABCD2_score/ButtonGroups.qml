import QtQuick 2.6
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

Item {
    
    ButtonGroup {
        id: aGroup
        buttons: aBox.aLayout.children
        onClicked: {
            logic.a = (button.text == aBox.no) ?0 :1;
            logic.recalculate()
        }
    }
    
    ButtonGroup {
        id: bGroup
        buttons: bBox.bLayout.children
        onClicked: {
            logic.b = (button.text == bBox.no) ?0 :1;
            logic.recalculate()
        }
    }
    
    ButtonGroup {
        id: cGroup
        buttons: cBox.cLayout.children
        onClicked: {
            if (button.text == cBox.weak) {
                logic.c = 2;
            }
            else if (button.text == cBox.speech) {
                logic.c = 1;
            }
            else {
                logic.c = 0;
            }
            logic.recalculate()
        }
    }
    
    ButtonGroup {
        id: d1Group
        buttons: d1Box.d1Layout.children
        onClicked: {
            if (button.text == d1Box.over60) {
                logic.d1 = 2;
            }
            else if (button.text == d1Box.under60) {
                logic.d1 = 1;
            }
            else {
                logic.d1 = 0;
            }
            logic.recalculate()
        }
    }
    
    ButtonGroup {
        id: d2Group
        buttons: d2Box.d2Layout.children
        onClicked: {
            logic.d2 = (button.text == d2Box.no) ?0 :1;
            logic.recalculate()
        }
    }
    
}
